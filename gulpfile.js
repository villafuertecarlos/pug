/* Config Gulp Task */
var gulp = require('gulp'),
	nib = require('nib'),
	swig = require('gulp-swig'),
	connect = require('gulp-connect'),
	concat = require('gulp-concat'),
	stylus = require('gulp-stylus'),
	plumber = require('gulp-plumber'),
	findPort = require('find-port'),
	jade = require('gulp-jade'),
	pug = require('gulp-pug'),
	babel = require('gulp-babel');

API_KEY = 'AIzaSyByIGziYy98iJBgwI3RPXBraL54WD0Eofo';

if(API_KEY){
	GOOGLE_APIKEY = '&key='+API_KEY;
}else{
	GOOGLE_APIKEY = '&sensor=false';
}

function StartsWith(s1, s2) {
  return (s1.length >= s2.length && s1.substr(0, s2.length) == s2);
}

//Variables de Templates
var server_port = 8080;
findPort(server_port, server_port+10, function(ports) {
		server_port = ports[0];
	});

//Directorios de sistema
var path = {
	pug: ['source/templates/*.pug','source/templates/common/*.pug'],

	pug_blocks: ['source/templates/blocks/*.pug'],

	pug_build: 'source/app',

	pug_build_blocks: 'source/app/blocks/',

	// creando solo header y footer
	pug_header_footer: ['source/templates/common/header.pug', 'source/templates/common/footer.pug'],

	html_header_and_footer : 'source/app/header_and_footer',

	master_styl: 'source/static/stylus/styles.styl',

	production_styl: 'source/static/stylus/production.styl',

	stylus_dirs: 'source/static/stylus/**/*.styl',

	stylus_blocks_dir: 'source/static/stylus/blocks/*.styl',

	css_builds: 'source/static/css/builds/*.css',

	css_builds_dir: 'source/static/css/builds/',

	css: 'source/static/css/'

}
CONST = {
	ROOT : 'source/'
};

//Livereload - Watch Taks HTML - CSS
gulp.task('connect', function(){
	setTimeout(function () {
		connect.server({
			root: CONST.ROOT,
			host:'0.0.0.0',
			port: server_port,
			livereload: true
		});
	}, 600)
});

gulp.task('builder', function() {
	console.log("[00:00:00] Compilando html's on /app/*.html");
	var templates_vars = {
		load_json: true,
		defaults: {
			cache: false
		},
		data: {
			STATIC_URL : '../static/',
			PROD_CSS:'',
			GOOGLE_APIKEY: GOOGLE_APIKEY
		}
	}
	return gulp.src(path.pug)
	.pipe(plumber())
	.pipe(swig(templates_vars))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.pug_build))});

// Reload Server Function
gulp.task('reload_server', function () {
	// Se retrasa a 0.2 segundos el LiveReload
	setTimeout(function () {
		return gulp.src(path.pug_build).pipe(connect.reload()).on('end', function(){
				console.log('>>>>>>>>>> Navegador refrescado...');
			});
	}, 768);
});

// Concat Css
gulp.task('concat_css', function () {
	setTimeout(function () {
		return gulp.src(path.css_builds)
		.pipe(plumber())
		.pipe(concat('blocks_styl.css'))
		.pipe(plumber.stop())
		.pipe(gulp.dest(path.css)).on('end', function(){
				console.log('>>>>>>>>>> Css Concatenados perfectamente...');
			})
	}, 256);
});

// Stylus Compiler
gulp.task('stylus', function () {
	return gulp.src(path.master_styl)
	.pipe(plumber())
	.pipe(stylus({ use: nib(), compress: true, import: ['nib']}))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.css))});

// Stylus Compiler
gulp.task('stylus_prod', function () {
	return gulp.src(path.production_styl)
	//.pipe(plumber())
	.pipe(stylus({ compiler: true, use: nib(),  import: ['nib']}))
	//.pipe(plumber.stop())
	.pipe(gulp.dest(path.css))});

// Stylus Compiler
gulp.task('stylus_blocks', function () {
	return gulp.src(path.stylus_blocks_dir)
	.pipe(plumber())
	.pipe(stylus({ use: nib(), compress: true, import: ['nib','../config/identity']}))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.css_builds_dir)).on('end', function(){
		setTimeout(function () {
			return gulp.src(path.css_builds)
			.pipe(plumber())
			.pipe(concat('blocks_styl.css'))
			.pipe(plumber.stop())
			.pipe(gulp.dest(path.css)).on('end', function(){
				console.log('>>>>>>>>>> Css Concatenados perfectamente...');
			})
		}, 1);
	})});

// pug Compiler Function
gulp.task('pug', function() {
	var build_vars = {
		STATIC_URL:'../static/',
		GOOGLE_APIKEY: GOOGLE_APIKEY
	}
	return gulp.src(path.pug)
	.pipe(plumber())
	.pipe(pug({
		pretty: true,
		locals: build_vars
	}))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.pug_build))
});

// COMPILANDO HEADER Y FOOTER
gulp.task('pug_header_footer', function() {
	var build_vars = {
		STATIC_URL:'../static/',
		GOOGLE_APIKEY: GOOGLE_APIKEY
	}
	return gulp.src(path.pug_header_footer)
	.pipe(plumber())
	.pipe(pug({
		pretty: true,
		locals: build_vars
	}))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.html_header_and_footer))
	console.log("[00:00:00] Compilando ['header and Footer'] in ", path.html_header_and_footer);
});

gulp.task('pug_blocks', function() {
	console.log("[00:00:00] Compilando blocks's on /app/blocks/*.pug");
	var build_vars = {
		STATIC_URL:'../../static/',
		GOOGLE_APIKEY: GOOGLE_APIKEY
	}
	return gulp.src(path.pug_blocks)
	.pipe(plumber())
	.pipe(pug({
		pretty: true,
		locals: build_vars
	}))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.pug_build_blocks))
	console.log('>>>>>>><cambios compilados..... ;)');
});

function styl_com_con(file){
	if (StartsWith(file.path,"/")){
		file_name = file.path.split('/')[file.path.split('/').length - 1];
	}else{
		file_name = file.path.split('\\')[file.path.split('\\').length - 1];
	}
	console.log('>>>>>>>>>> Compiling '+file_name+' : running tasks...');
	gulp.src(file.path)
	.pipe(plumber())
	.pipe(stylus({ use: nib(),  import: ['nib','../config/identity']}))
	.pipe(plumber.stop())
	.pipe(gulp.dest(path.css_builds_dir))
	console.log('>>>>>>>>>> Compiled! '+file_name);
}

// watchers filePaths #withReload
gulp.task('watch', function () {
	gulp.watch(path.stylus_blocks_dir, ['concat_css']).on('change', function(file) {
		return styl_com_con(file);
	});
	gulp.watch(path.stylus_dirs, ['stylus','reload_server']);
	gulp.watch(path.pug, ['pug','reload_server']);
	gulp.watch(path.pug_blocks, ['pug_blocks','reload_server']);
	gulp.watch(path.pug_header_footer, ['pug_header_footer','reload_server']);
});

// // StartServer and CompilerStylus
gulp.task('default', ['stylus_blocks','pug','pug_blocks','pug_header_footer','stylus','watch','connect']);

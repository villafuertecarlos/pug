'use strict';

$(function(){
	//detectando tablet, celular o ipad
	const isMob2 = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		// tasks to do if it is a Mobile Device
		function readDeviceOrientation() {
			if (Math.abs(window.orientation) === 90) {
				// Landscape
				// cerrar_nav();
			} else {
				// Portrait
				// cerrar_nav();
			}
		}
		window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function(){
			// var estadomenu = document.querySelector('.menu_responsive').innerWidth;
			// if(estadomenu != 0){
			// 	// cerrar_nav();
			// }
		});
	}
})
